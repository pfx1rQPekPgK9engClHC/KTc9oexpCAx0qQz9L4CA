// ==UserScript==
// @name          Plantilla PixelCanvas
// @namespace     https://nido.org/b
// @version       2017.7.20.22.23
// @description   Autismo
// @author        Choroy
// @match         http://pixelcanvas.io/*
// @grant         none
// @updateURL     https://pfx1rqpekpgk9engclhc.gitlab.io/KTc9oexpCAx0qQz9L4CA/canvas.js
// @downloadURL   https://pfx1rqpekpgk9engclhc.gitlab.io/KTc9oexpCAx0qQz9L4CA/canvas.js
// @require       https://code.jquery.com/jquery-3.2.1.min.js
// ==/UserScript==

(function(){
	var gameCanvas = document.getElementById("gameWindow");
	var coordsContainer = document.getElementById("app").children[0].children[8].children[1].children[0];
	var imgContainer = $("<div style=\"position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; pointer-events: none; overflow: hidden\"></div>");
	var coordsHandler;
	var readCoords = [0, 0];
	var readingCoords = false;
	var mouseDown = false;
	var inCanvasUpdate = false;
	var webRoot = "https://pfx1rqpekpgk9engclhc.gitlab.io/KTc9oexpCAx0qQz9L4CA/";
	var images = {};
	var showTemplates = true;
	var canvasData = {visible: [[], []], scale: 0, ref: [[], []]};

	function onLoad()
	{
		$("body").append(imgContainer);
		imgContainer.css("z-index", "99");
		$("#app > div > div").css("z-index", "999");
		$("#app > div > div > div").css("z-index", "999");
		window.setTimeout(function() { $(".g-recaptcha").find("div").css("z-index", "999"); }, 2500);

		$($("#app > div > div").not(function(i, e) { return e.className.length || e.id.length; } )[1]).click(toggleTemplates);

		updateCanvas();
		fetchImages();
	}

	function updateCanvas(force = false)
	{
		inCanvasUpdate = true;

		var rect = gameCanvas.getBoundingClientRect();
		var newTLCoords = posToCanvas(rect.left, rect.top);
		var newBRCoords = posToCanvas(rect.right, rect.bottom);
		var newReference = detectTopLeft(rect);

		if(force || newTLCoords !== canvasData.visible[0] || newBRCoords !== canvasData.visible[1] || newReference !== canvasData.ref)
		{
			canvasData.visible = [newTLCoords, newBRCoords];
			canvasData.ref = newReference;

			$.each(images,
				function(src, img)
				{
					var rectCanvas = {left: newTLCoords[0], right: newBRCoords[0], top: newBRCoords[1], bottom: newTLCoords[1]};
					var rectImage = {left: img.pos[0], right: img.pos[0] + img.size[0], top: img.pos[1] + img.size[1], bottom: img.pos[1]};

					if(rectCanvas.left < rectImage.right && rectCanvas.right > rectImage.left && rectCanvas.top > rectImage.bottom && rectCanvas.bottom < rectImage.top)
					{
						var topPos = (rectImage.bottom - newReference[1][1]) * canvasData.scale + newReference[0][1];
						var leftPos = (rectImage.left - newReference[1][0]) * canvasData.scale + newReference[0][0];

						img.element.css("top", topPos);
						img.element.css("left", leftPos);
						img.element.attr("width", img.size[0] * canvasData.scale + "px");
						img.element.attr("height", img.size[1] * canvasData.scale + "px");
						img.element.show();
					}
					else
					{
						img.element.hide();
					}
				}
			);
		}

		inCanvasUpdate = false;
	}

	function fetchImages()
	{
		$.ajax({
			url: webRoot + "/files.json",
			type: "GET",

			success:
			function(data)
			{
				data = JSON.parse(data);

				$.each(data,
					function(i, img)
					{
						var elem = $("<img></img>");
						images[img.src] = {ready: false, pos: img.pos, size: img.size, element: elem};

						elem.on("load", function() { images[img.src].ready = true; });
						elem.attr("src", webRoot + img.src);
						elem.css("position", "absolute");
						elem.hide();

						imgContainer.append(elem);
					}
				);

				updateCanvas(true);
			},

			cache: true,
			processData: false
		});
	}

	function toggleTemplates(event)
	{
		showTemplates = !showTemplates;
		event.stopPropagation();

		if(showTemplates)
		{
			imgContainer.show();
		}
		else
		{
			imgContainer.hide();
		}
	}

	function onCoordsUpdate()
	{
		if(readingCoords)
		{
			coordsData = coordsContainer.innerHTML.slice(1, -1).split(", ");
			readCoords[0] = +coordsData[0];
			readCoords[1] = +coordsData[1];
		}
	}

	function posToCanvas(x, y)
	{
		var event = new MouseEvent("mousemove", {isTrusted: true, view: window, bubbles: true, cancelable: true, clientX: x, clientY: y, x: x, y: y});

		readingCoords = true;
		gameCanvas.dispatchEvent(event);
		readingCoords = false;

		return [readCoords[0], readCoords[1]];
	}

	function detectTopLeft(rect)
	{
		var initialPos;
		var currentX;
		var currentY;

		initialPos = posToCanvas(rect.left, rect.top);
		currentX = rect.left + 1;
		currentY = rect.top + 1;

		while(true)
		{
			pos = posToCanvas(currentX, currentY);

			if(pos[0] != initialPos[0] || currentX >= rect.right)
				break;

			currentX += 1;
		}

		while(true)
		{
			pos = posToCanvas(currentX, currentY);

			if(pos[1] != initialPos[1] || currentY >= rect.bottom)
				break;

			currentY += 1;
		}

		return [[currentX, currentY], [initialPos[0] + 1, initialPos[1] + 1]];
	}

	function detectPixelSize(rect)
	{
		var centerX = Math.round((rect.right - rect.left) / 2.0);
		var centerY = Math.round((rect.bottom - rect.top) / 2.0);
		var canvasCenter;
		var currentLeft;
		var currentRight;

		canvasCenter = posToCanvas(centerX, centerY);
		currentLeft = centerX - 1;
		currentRight = centerX + 1;

		while(true)
		{
			pos = posToCanvas(currentLeft, centerY);

			if(pos[0] != canvasCenter[0] || currentLeft <= rect.left)
				break;

			currentLeft -= 1;
		}

		while(true)
		{
			pos = posToCanvas(currentRight, centerY);

			if(pos[0] != canvasCenter[0] || currentRight >= rect.right)
				break;

			currentRight += 1;
		}

		return currentRight - currentLeft - 1;
	}

	function onMouseDown()
	{
		mouseDown = true;
	}

	function onMouseUp()
	{
		mouseDown = false;
	}

	function onMouseMove()
	{
		if(mouseDown && !inCanvasUpdate)
			updateCanvas();
	}

	function onMouseClick()
	{
		if(canvasData.scale < 10)
			updateCanvas();
	}

	var _scale = CanvasRenderingContext2D.prototype.scale;

	CanvasRenderingContext2D.prototype.scale =
		function()
		{
			if(canvasData.scale != arguments[0])
			{
				canvasData.scale = arguments[0];
				updateCanvas(true);
			}
			return _scale.apply(this, arguments);
		};

	coordsContainer.addEventListener("DOMSubtreeModified", onCoordsUpdate);
	window.addEventListener("resize", updateCanvas);
	gameCanvas.addEventListener("mousedown", onMouseDown);
	gameCanvas.addEventListener("mouseup", onMouseUp);
	gameCanvas.addEventListener("mousemove", onMouseMove);
	gameCanvas.addEventListener("click", onMouseClick);

	onLoad();
})();
