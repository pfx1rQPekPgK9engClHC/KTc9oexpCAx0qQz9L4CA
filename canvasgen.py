#!/usr/bin/env python3

import os
import math
import json
import threading
import subprocess

from pyx import *
from PIL import Image

colors = [
	(255, 255, 255),	(228, 228, 228),	(136, 136, 136),	(34, 34, 34),
	(255, 167, 209),	(229, 0, 0),	(229, 149, 0),	(160, 106, 66),
	(229, 217, 0),	(148, 224, 68),	(2, 190, 1),	(0, 211, 221),
	(0, 131, 199),	(0, 0, 234),	(207, 110, 228),	(130, 0, 128)
]

fileList = []
webFileList = []
threadList = []
constMaxThreads = 8

with open("files.json") as fileListF:
	fileList = json.load(fileListF)

def similarPixel(pixel):
	aux = {((c[0]-pixel[0])**2 + (c[1]-pixel[1])**2 + (c[2]-pixel[2])**2): c for c in colors}
	return aux[min(aux)]

def compareTimes(src, svg, svgz, svgf, svgzf):
	st1 = os.stat(src)
	st2 = os.stat(svg)
	st3 = os.stat(svgz)
	st4 = os.stat(svgf)
	st5 = os.stat(svgzf)
	return (st3.st_mtime >= st2.st_mtime > st1.st_mtime) and (st5.st_mtime >= st4.st_mtime > st1.st_mtime)

def convertImage(origImg, pixelSize, svgFile, svgFile2, outFile, outFile2):
	nullFile = open(os.devnull, "w")
	normW = int(math.ceil(origImg.width / float(pixelSize)))
	normH = int(math.ceil(origImg.height / float(pixelSize)))

	outCanvas = canvas.canvas()
	filledCanvas = canvas.canvas()

	for x in range(0, normW):
		for y in range(0, normH):
			pix = origImg.getpixel( (x * pixelSize, y * pixelSize) )

			if pix not in colors:
				pix = similarPixel(pix)

			destX = x * 8
			destY = (normH - y) * 8

			p = path.rect(destX + 0.5, destY + 0.5, 7, 7)
			outCanvas.stroke(p, [color.rgb(pix[0] / 255.0, pix[1] / 255.0, pix[2] / 255.0), style.linewidth(unit.w_pt)])

			p = path.rect(destX, destY, 8, 8)
			outCanvas.stroke(p, [color.rgb(0, 0, 0), style.linewidth(0.5 * unit.w_pt)])

			filledCanvas.fill(p, [color.rgb(pix[0] / 255.0, pix[1] / 255.0, pix[2] / 255.0)])
			filledCanvas.stroke(p, [color.rgb(0, 0, 0), style.linewidth(0.5 * unit.w_pt)])

	outCanvas.writetofile(svgFile)
	filledCanvas.writetofile(svgFile2)

	subprocess.call(["inkscape", "-z", svgFile, "-e", outFile, "-D", "-w", str(normW*40), "-h", str(normH*40)], stdout=nullFile, stderr=nullFile)
	subprocess.call(["inkscape", "-z", svgFile2, "-e", outFile2, "-D", "-w", str(normW*10), "-h", str(normH*10)], stdout=nullFile, stderr=nullFile)

	origImg.close()
	nullFile.close()

unit.set(defaultunit="pt")

while len(fileList):
	i = 0

	while i < len(threadList):
		if not threadList[i].isAlive():
			del threadList[i]
		else:
			i += 1

	if len(threadList) >= constMaxThreads:
		time.sleep(0.01)
		continue

	f = fileList.pop(0)

	fileName = f["file"]
	baseName = ".".join(fileName.replace("\\", "/").split("/")[-1].split(".")[:-1]).lower().replace(" ", "_")
	tempFile = "svg/" + baseName + ".svg"
	tempFile2 = "svg/" + baseName + "_filled.svg"
	finalFile = "img/" + baseName + ".png"
	finalFile2 = "img/" + baseName + "_filled.png"
	origImg = Image.open(fileName)

	webFileList.append({"pos": f["pos"], "size": [int(math.ceil(origImg.width / float(f["scale"]))), int(math.ceil(origImg.height / float(f["scale"])))], "src": finalFile})
	finalFile = "public/" + finalFile

	if os.path.exists(tempFile) and os.path.exists(finalFile) and os.path.exists(tempFile2) and os.path.exists(finalFile2) and compareTimes(fileName, tempFile, finalFile, tempFile2, finalFile2):
		origImg.close()
		continue

	print("> Converting \"" + fileName + "\"")

	workThread = threading.Thread(target=convertImage, args=(origImg, f["scale"], tempFile, tempFile2, finalFile, finalFile2))
	workThread.start()
	threadList.append(workThread)

for t in threadList:
	t.join()

with open("public/files.json", "w") as outFileList:
	json.dump(webFileList, outFileList)
